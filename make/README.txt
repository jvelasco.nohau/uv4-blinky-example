Example built using uVision
to be run on a Windows OS machine

You can run this example by invoking make. 
To run this example

1) Create codesonar-project directory. This is to keep the directory clean
2) Go into codesonar-project/
3) Open a Windows PowerShell
4) Run codesonar analyze -clean local_make_blinky 127.0.0.1:7341 make -C ..